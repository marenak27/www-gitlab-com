---
layout: handbook-page-toc
title: "zapier"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# What is Zapier?
Zapier is an automation tool that allows us to move data from one application to another, that otherwise would not have had an integration. Zapier helps us be more efficient and allow for real-time passage of data between two disperate systems.

**Common Zaps**:
* Google Sheets to Marketo
* Facebook Lead Ads to Marketo
* EventBrite to Marketo

# What Data is Required?
These fields must be required and captured in order to allow for proper routing of leads:
* First Name
* Last Name
* Email
* Company
* Country (must be a [picklist containing all or some of these values](https://docs.google.com/spreadsheets/d/1cV_hI2wAzLxYYDI-NQYF5-FDDPXPXH0VV5qRBUJAQQk/edit?usp=sharing))
* Number of employees (Can be range, or number)
* Email Consent (Whether or not we recieved consent to send email, can be `TRUE` of `FALSE`) [Reference the legal handbook](/handbook/legal/marketing-collaboration/#marketing-rules-and-consent-language) for the language that should be used.



## When to Request 
This rubric will help you determine if Zapier is a viable solution for your campaign.  It will be at Mops discretion whether or not Zapier is the right solution for your campaign. Mops does not own Zapier, but we have access to use the tool to help with programs and campaigns. We are not responsible for setting up zaps for other reasons.

Mops evaluates and triages incoming requests every Monday and that is when your request will be accepted/rejected. Please allow 2-3 weeks for Mops to build the Zap. The Zap should be set up *before* the program is set to go live.

There are two main reasons and advantages of using Zapier:
1. When there is a time savings for setting up the Zap vs importing multiple lists
1. Having data pass between systems in real-time


|Type|Example|Zap?|
|----|----|-----|
|Recurring list loads that happen over course of weeks/months|Ongoing survey campaign |Usually|
|Registration over the course of weeks/months outside of Marketo|Commit|Sometimes|
|Form Automation when connector to Marketo not available|EventBrite, Social Forms|Usually|
|One time list loads |Sponsored Webcast                        |No|

## How to Request 
Please submit an issue with Marketing Operations using this [issue template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/blob/master/.gitlab/issue_templates/zapier_connection_request.md)

If you are using a google form, you must give mops access to the google sheet containing the responses from the form.
