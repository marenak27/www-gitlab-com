---
layout: handbook-page-toc
title: "Security Shadow Program"
---

### On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Security Shadow Program
From converging on real-time critical events with SIRT, exploiting vulnerabilities with the Red Team or participating in live Customer Assurance calls with the Risk and Field Security team, you will have the opportunity to work next to security staff to gain valuable insight and working knowledge of security fundamentals across multiple domains.

## Course Catalog
Our course catalog is divided into three main areas in alignment with the three [Sub-Organizations](https://about.gitlab.com/handbook/engineering/security/#department-structure) within the Security Department. 

- [Security Engineering and Research](sites/handbook/source/handbook/engineering/security/security-shadow-sec-eng-res.html.md)
- [Security Operations](sites/handbook/source/handbook/engineering/security/security-shadow-security-operations.html.md)
- [Security Assurance](sites/handbook/source/handbook/engineering/security/security-shadow-security-assurance.html.md)

## Enrollment

Ready to embark on this adventure with us? 

1. Talk to your manager. Make sure they approve the time you will need to dedicate to this program. 
1. Join the [#security-department](https://gitlab.slack.com/archives/CM74JMLTU) Slack Channel
1. Click on the blue lightning bult in the bottom left corner to initiate the Security Shadow Program Workflow
1. Compelte the Enrollment Form
1. The manager of the applicable Security Team will reach out to schedule your start date


## Questions
Feel free to contact the [Security Team](https://gitlab.slack.com/archives/CM74JMLTU) in our Slack Channel


 
