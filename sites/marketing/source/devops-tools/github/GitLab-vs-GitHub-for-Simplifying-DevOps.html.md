---
layout: markdown_page
title: "GitLab vs GitHub for Simplifying DevOps"
description: "Learn more about Gitlab capabilities missing in Github related to securing the DevOps Process and Compliance."
canonical_path: "/devops-tools/github/GitLab-vs-GitHub-for-Simplifying-DevOps.html"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab Capabilities Missing in GitHub related to Securing the DevOps Process

|                                              |                             GitLab Capability                            |                                                                                                                                                              Features                                                                                                                                                             |
|----------------------------------------------|:------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|                           Single Touch Point |              Maintain Confidentiality of issues and activity             |                                                                                               Issues marked as Confidential, Private Profile Pages (Activity related info disabled in profile settings for certain sensitive users)                                                                                               |
| Technical Support for Production Environment | Fine grained controls on who can access repository, submit code, deploy. | Granular user roles and flexible permissions (five different user roles and settings for external users, Set permissions according to people’s role, rather than either read or write access to a repository.), Reject Unsigned Commits, Verified Committer, Protected Environments (Control who can deploy to which environment) |
|                      Cross Product Knowledge |                       Securely access remote assets                      |                                                                                                                                  Proxy remote package registries for safer, more reliable builds                                                                                                                                  |

## GitLab Compliance Capabilities Missing in GitHub
    
- Financial Services Regulatory Compliance

| GitLab Capability                                           | Features                                                                                                                                                                                       |
|-------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Segregation of Incompatible Duties (SODs)                   | Defined Project Permissions, Protected branches, Protected environments, Merge request approvals, Unprotect permission                                                                         |
| Identity and Access Approval Controls to Ensure Proper SODs | Role-Based Access Controls (RBAC) within protected branches and environments.                                                                                                                  |
| Configuration Management & Change Control                   | CI-CD Configurations, CI/CD pipeline configuration management, Audit Events                                                                                                                    |
| Auditing                                                    | One concept of a user across the lifecycle to ensure the right level of permissions and access, Audit logs, Audit events, Container image retention, Artifact retention, Test result retention |

- Other Compliances

| GitLab Capability                                    | Features                                                                                                                                                    |
|------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| PCI Compliance                                       | GitLab addresses application security, which is a critical element for the enterprise wishing to be PCI-compliant.                                          |
| HIPPA                                                | Identify and manage risks and vulnerabilities, Define and enforce development standards and processes                                                       |
| GDPR                                                 | Membership locking, rejecting unsigned commits, <br>user permissions, push rules etc. prevent sensitive files from accidentally being pushed to production. |
| IEC 62304:2006<br>ISO 13485:2016<br>ISO 26262-6:2018 | Creating and documenting plans and processes, maintaining end to end traceability etc. help support these compliance needs                                  |

